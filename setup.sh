#!/bin/bash

###################################################################################
# This was tested only on fedora/centos machines                                  #
# some system path or service names may differ on debian distro                   #
###################################################################################

CURRENT_DIR=$(readlink -f $(dirname ${BASH_SOURCE[0]}))

###################################################################################
# These are the variable you are supposed to change                               #
###################################################################################
SERVER_NAME="localhost"
GUNICORN_WORKERS="4"
APPLICATION_NAME="Mitfahrgelegenheit"
APPLICATION_REPO="https://github.com/Chedi/CarSharing.git"

SSL_SUBJECT="/C=TN/ST=Tunis/L=Tunis/O=Epsilon tech/OU=R&D/CN=localhost/emailAddress=contact@epsilon.tn"

###################################################################################
# Edit the bellow only if you know what you are doing  ;-)                        #
###################################################################################
SSL_DIR="$CURRENT_DIR/ssl/"
RUN_DIR="$CURRENT_DIR/run/"
SSL_KEY="$SSL_DIR/server.key"
SSL_CRT="$SSL_DIR/server.crt"
LOGS_DIR="$CURRENT_DIR/logs/"
MEDIA_DIR="$CURRENT_DIR/$APPLICATION_NAME/media/"
STATIC_DIR="$CURRENT_DIR/$APPLICATION_NAME/static/"
VIRTUAL_ENV_DIR="$CURRENT_DIR/ve/"
GUNICORN_SOCKET="unix:$CURRENT_DIR/run/gunicorn.sock"
GUNICORN_SCRIPT="$CURRENT_DIR/$APPLICATION_NAME/gunicorn.sh"

echo "generating nginx configuration file"
sed -e "s;%SSL_CRT%;$SSL_CRT;g"                 \
    -e "s;%SSL_KEY%;$SSL_KEY;g"                 \
    -e "s;%LOGS_DIR%;$LOGS_DIR;g"               \
    -e "s;%MEDIA_DIR%;$MEDIA_DIR;g"             \
    -e "s;%STATIC_DIR%;$STATIC_DIR;g"           \
    -e "s;%SERVER_NAME%;$SERVER_NAME;g"         \
    -e "s;%GUNICORN_SOCKET%;$GUNICORN_SOCKET;g" \
    $CURRENT_DIR/server_config/app.conf.tpl > $CURRENT_DIR/server_config/$APPLICATION_NAME.conf

echo "generating supervisor configuration file"
sed -e "s;%LOGS_DIR%;$LOGS_DIR;g"                 \
    -e "s;%GUNICORN_SCRIPT%;$GUNICORN_SCRIPT;g"   \
    -e "s;%VIRTUAL_ENV_DIR%;$VIRTUAL_ENV_DIR;g"   \
    -e "s;%APPLICATION_NAME%;$APPLICATION_NAME;g" \
    $CURRENT_DIR/server_config/supervisor.ini.tpl > $CURRENT_DIR/server_config/$APPLICATION_NAME.ini

echo "generating gunicorn script file"
sed -e "s;%GUNICORN_WORKERS%;$GUNICORN_WORKERS;g" \
    -e "s;%APPLICATION_NAME%;$APPLICATION_NAME;g" \
    $CURRENT_DIR/server_config/gunicorn.sh.tpl > $CURRENT_DIR/server_config/gunicorn.sh

if [[ ! -d "$VIRTUAL_ENV_DIR" ]]; then
  echo "creating the virtual environment folder"
  virtualenv --no-site-packages $VIRTUAL_ENV_DIR
fi

echo "reproducing the project structure"
mkdir -p $SSL_DIR
mkdir -p $RUN_DIR
mkdir -p $LOGS_DIR

if [[ ! -f SSL_KEY || ! -f SSL_CRT ]]; then
  echo "generating self signed ssl keys"
  cd $SSL_DIR
  openssl genrsa -passout pass:pass -des3 -out server.key 1024
  openssl req -passin pass:pass -new -key server.key -out server.csr -subj "$SSL_SUBJECT"
  cp -f server.key server.key.org
  openssl rsa -passin pass:pass -in server.key.org -out server.key
  openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt
  cd $CURRENT_DIR
fi

echo "loading virtual environment"
source $CURRENT_DIR/ve/bin/activate

echo "checking the django application $APPLICATION_NAME"
if [[ ! -d $CURRENT_DIR/$APPLICATION_NAME ]]; then
  echo "application not present, cloning from repository"
  git clone $APPLICATION_REPO $APPLICATION_NAME
fi

echo "requirement check"
pip install -r $CURRENT_DIR/$APPLICATION_NAME/requirements.txt

echo "reloading supervisor configuration"
sudo mv $CURRENT_DIR/server_config/gunicorn.sh $CURRENT_DIR/$APPLICATION_NAME/ -f
sudo mv $CURRENT_DIR/server_config/$APPLICATION_NAME.ini /etc/supervisord.d/ -f
chmod +x $GUNICORN_SCRIPT
sudo service supervisord restart

echo "reloading nginx configuration"
sudo mv $CURRENT_DIR/server_config/$APPLICATION_NAME.conf /etc/nginx/conf.d/ -f
sudo service nginx restart